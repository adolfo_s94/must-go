## MustGo
#MustGo - NativeScript Application

Must Go, los lugares que tienes que conocer

Navega en nuestra plataforma y encuentra desde restaurantes y bares hasta farmacias y estacionamientos. Must Go te guiara hasta que encuentres el lugar que se adapte a tus planes, basado en comentarios, calificaciones, fotos y filtros específicos que te ayudaran a encontrar mas rápido lo que estas buscando. Comparte en redes sociales, comenta tu experiencia y salva tus lugares preferidos en Must Go.

*Adolfo Solis Rosas - adolfo2794@gmail.com
