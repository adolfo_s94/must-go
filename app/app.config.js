var config = {
    FACEBOOK_GRAPH_API_URL: "https://graph.facebook.com/v2.9"
};
exports.config = config;


var configMaps = {
    GEOCODING_API_KEY: "AIzaSyCm6uQuuhqyByJUrNLy3yEuCZM_lF-dSU4"
};
exports.configMaps = configMaps;


function removeDuplicates(arr, key) {
    if (!(arr instanceof Array) || key && typeof key !== 'string') {
        return false;
    }

    if (key && typeof key === 'string') {
        return arr.filter(function(obj, index, arr) {
            return arr.map(function(mapObj) {
                return mapObj[key];
            }).indexOf(obj[key]) === index;
        });
    } else {
        return arr.filter(function(item, index, arr) {
            return arr.indexOf(item) == index;
        });
    }
}
exports.removeDuplicates = removeDuplicates;