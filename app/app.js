require("./bundle-config");

var applicationModule = require("application");
var firebase = require("nativescript-plugin-firebase");
var appSettings = require("application-settings");
var geolocation = require("nativescript-geolocation");
firebase.init({
    // Optionally pass in properties for database, authentication and cloud messaging,
    // see their respective docs.
    persist: true,
    iOSEmulatorFlush: true,
    storageBucket: 'gs://must-go-1499117215424.appspot.com',
    onAuthStateChanged: function(data) { // optional but useful to immediately re-logon the user when he re-visits your app
        console.log(data.loggedIn ? "Logged in to firebase" : "Logged out from firebase");
        if (data.loggedIn) {
            console.log("user's email address: " + (data.user.email ? data.user.email : "N/A"));
        }
    }
}).then(
    function(instance) {
        console.log("firebase.init done");
        var location = geolocation.getCurrentLocation({ desiredAccuracy: 3, updateDistance: 10, maximumAge: 20000, timeout: 20000 }).
        then(function(loc) {
            if (loc) {
                console.log("Location setted")
                appSettings.setNumber("currentLocationLat", loc.latitude);
                appSettings.setNumber("currentLocationLong", loc.longitude);
            }
        }, function(e) {
            console.log("Error: " + e.message);
        });
    },
    function(error) {
        console.log("firebase.init error: " + error);
    }
);

if (applicationModule.ios) {
    GMSServices.provideAPIKey("AIzaSyCm6uQuuhqyByJUrNLy3yEuCZM_lF-dSU4");
}


if (appSettings.getString("username") != null || appSettings.getString("username") != undefined)
    applicationModule.start({ moduleName: "views/home/home" });
else
    applicationModule.start({ moduleName: "views/login/login" });