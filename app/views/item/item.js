var Directions = require("nativescript-directions").Directions;
var directions = new Directions();
var app = require("application");
var color = require("color");
var platform = require("platform");
var view = require("ui/core/view");
var SocialShare = require("nativescript-social-share");
var frameModule = require("ui/frame");
var utilsModule = require("tns-core-modules/utils/utils");
var config = require("~/app.config").configMaps;
var http = require("tns-core-modules/http");
var mapsModule = require("nativescript-google-maps-sdk");
var appSettings = require("application-settings");
var phone = require("nativescript-phone");
var dialogs = require("ui/dialogs");
var observableArray = require("data/observable-array");
var PhotoViewer = require("nativescript-photoviewer");
var SnackBar = require("nativescript-snackbar");
var snackbar = new SnackBar.SnackBar();
var platformModule = require("tns-core-modules/platform");
var striptags = require('striptags');
var firebase = require("nativescript-plugin-firebase");
var arraySort = require('array-sort');

var array = new observableArray.ObservableArray();
var comments = new observableArray.ObservableArray();
var page;
var gotData;
var fav;
var coordx;
var coordy;
var title;

var myImages;


var width;
var height;

// Address
var domicilio


// Comment
var tvComment;
var stackLayoutComments;
var labelNoComment;

exports.pageLoaded = function(args) {
    page = args.object;
    gotData = page.navigationContext;

    photoViewer = new PhotoViewer(); // GALLERY

    array.splice(0, array.length);

    drawer = view.getViewById(page, "sideDrawer");

    var actionBar = view.getViewById(page, "actionBar");

    var imgHeader = view.getViewById(page, "imgHeader");
    var icon = view.getViewById(page, "icon");
    title = view.getViewById(page, "title");
    var path = view.getViewById(page, "path");
    var stars = view.getViewById(page, "stars");
    var price = view.getViewById(page, "price");
    domicilio = view.getViewById(page, "domicilio");
    var description = view.getViewById(page, "description");
    fav = view.getViewById(page, "fav");

    tvComment = view.getViewById(page, "tvComment")
    btnComment = view.getViewById(page, "btnComment")

    stackLayoutComments = view.getViewById(page, "stackLayoutComments")
    labelNoComment = view.getViewById(page, "labelNoComment")

    var iconProfile = view.getViewById(page, "iconProfile");
    iconProfile.backgroundImage = appSettings.getString('profileimage');


    width = platformModule.screen.mainScreen.widthDIPs;
    height = platformModule.screen.mainScreen.heightDIPs;

    drawer.drawerContentSize = height / 3.5;

    icon.height = height / 10;
    icon.width = icon.height;

    icon.marginTop = 140 - icon.height / 2;
    icon.marginLeft = (width / 2) - (icon.width / 2);

    actionBar.title = gotData.title;

    imgHeader.backgroundImage = gotData.portada;
    icon.backgroundImage = gotData.imgProduct;

    title.text = gotData.title;
    path.text = gotData.path;
    stars.text = gotData.rating;
    price.text = gotData.price;
    description.text = gotData.description;
    appSettings.setString("phone", gotData.telefono)

    var cc = gotData.cc;
    var wifi = gotData.wifi;
    var park = gotData.estacionamiento;
    var horarios = striptags(gotData.horarios, [], '');
    var direccion = gotData.direccion
    var discapacitados = gotData.discapacitados
    var mascotas = gotData.mascotas
    var idiomas = gotData.idiomas
    var fumar = gotData.fumar
    var emergencias = gotData.emergencias
    var alberca = gotData.alberca

    coordx = gotData.coordx;
    coordy = gotData.coordy;

    appSettings.setNumber("coordx", Number(coordx));
    appSettings.setNumber("coordy", Number(coordy));

    if (cc == 1)
        array.push({ icn: String.fromCharCode(0xf09d), title: "\nPago con Tarjeta de Crédito", description: "Este establecimiento cuenta con pagos mediante tarjeta de crédito.\n" });
    if (wifi == 1)
        array.push({ icn: String.fromCharCode(0xf1eb), title: "\nWIFI", description: "Este establecimiento cuenta con internet inalámbrico.\n" });
    if (park == 1)
        array.push({ icn: String.fromCharCode(0xf1b9), title: "\nEstacionamiento", description: "Este establecimiento cuenta con estacionamiento privado.\n" });
    if (direccion == 1)
        array.push({ icn: String.fromCharCode(0xf0d1), title: "\nServicio a Domicilio", description: "Este establecimiento cuenta con el servicio de entrega a domicilio.\n" });
    if (discapacitados == 1)
        array.push({ icn: String.fromCharCode(0xf193), title: "\nLugar para Discapacitados", description: "Este establecimiento cuenta con lugar para discapacitados.\n" });
    if (mascotas == 1)
        array.push({ icn: String.fromCharCode(0xf1b0), title: "\nMascotas Permitidas", description: "Este establecimiento es amigable para tus mascotas.\n" });
    if (idiomas == 1)
        array.push({ icn: String.fromCharCode(0xf1ab), title: "\nOtros Idiomas", description: "Este establecimiento habla otros idiomas.\n" });
    if (fumar == 1)
        array.push({ icn: String.fromCharCode(0xf06d), title: "\nArea de Fumar", description: "Este establecimiento cuenta con area de fumar y no fumar.\n" });
    if (emergencias == 1)
        array.push({ icn: String.fromCharCode(0xf0fa), title: "\nKit de Emergencias", description: "Este establecimiento cuenta con kit de emergencias.\n" });
    if (alberca == 1)
        array.push({ icn: String.fromCharCode(0xf043), title: "\nAlberca", description: "Este establecimiento cuenta alberca, para que disfrutes al máximo.\n" });
    if (horarios)
        array.push({ icn: String.fromCharCode(0xf017), title: "\nHorarios", description: horarios + "\n" });



    var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + coordx + "," + coordy + "&key=" + config.GEOCODING_API_KEY;
    http.getJSON(url).then((res) => {
        location = res.results[0].formatted_address;
        domicilio.text = location;
    }, function(err) {
        console.log("Error getting location info: " + err);
    });

    var userID = appSettings.getString("USERID");
    var id = gotData.id;

    http.getJSON("http://mustgo.com.mx/api/likes/user/" + userID + "/negocio/" + id).then((res) => {
        http.getJSON("http://mustgo.com.mx/api/likes/user/" + userID + "/negocio/" + id).then((res) => {
            console.log("Like: " + res.liked)
            if (res.liked == true)
                fav.text = String.fromCharCode(0xf004) + String.fromCharCode(0x0a) + "Like" // LIKE
            else
                fav.text = String.fromCharCode(0xf08a) + String.fromCharCode(0x0a) + "Like" // DISLIKE
        }, function(err) {
            console.log("Error getting like info: " + err);
        });
    }, function(err) {
        console.log("Error getting like info: " + err);
    });

    var url = "http://mustgo.com.mx/api/negocios/" + id + "/visitas";
    http.getJSON(url).then((res) => {}, function(err) {
        console.log("Error setting visit: " + err);
    });

    myImages = [];

    myImages.push(gotData.portada);

    //Image from URLs (Android & iOS) 
    var url = "http://mustgo.com.mx/api/negocios/" + id + "/galeria";
    http.getJSON(url).then((res) => {
        console.log(res.length)
        for (let i = 0; i < res.length; i++) {
            myImages.push(res[i].url);
        }
    }, function(err) {
        console.log("Error setting images: " + err);
    });

    directions.available().then(
        function(avail) {
            console.log(avail ? "Yes" : "No");
        }
    );

    // FIREBASE QUERY COMMENTS
    queryFirebase()

    page.bindingContext = { details: array, comments: comments, zoom: 15, latitude: coordx, longitude: coordy, padding: 0, tilt: 0, bearing: 0 };
}


exports.onNavBtnTap = function() {
    console.log("Navigation button tapped!");
    frameModule.topmost().goBack();
}

exports.onShare = function() {
    SocialShare.shareText("http://mustgo.com.mx/detallesNegocio/" + gotData.id, gotData.title);
}

exports.comment = function() {
    utilsModule.openUrl(gotData.facebookPub);
}

exports.openMaps = function() {
    var coordx = appSettings.getNumber("coordx");
    var coordy = appSettings.getNumber("coordy");
    directions.navigate({
        to: { // either pass in a single object or an Array (see the TypeScript example below)
            address: domicilio.text
        }
        // for iOS-specific options, see the TypeScript example below.
    }).then(
        function() {
            console.log("Maps app launched.");
        },
        function(error) {
            console.log(error);
        }
    );
}

exports.call = function() {
    var tel = appSettings.getString("phone");
    var find = " ";
    var regex = new RegExp(find, "g");
    tel = tel.replace(regex, "-");
    phone.dial(tel, true);
}

exports.calificar = function() {
    dialogs.confirm({
        title: title.text,
        message: "Califica nuestro negocio, nos ayudará a mejorar.",
        okButtonText: "Me Gusta",
        cancelButtonText: "No me Gusta"
    }).then(function(result) {
        // result argument is boolean
        console.log("Dialog result: " + result);
        snackbar.simple('Gracias, tu calificación nos ayudará a mejorar.').then((args) => {
            this.set('jsonResult', JSON.stringify(args));
        })
    });
}

function bangThis(args) {

    var userID = appSettings.getString("USERID");

    if (userID != null) {

        var code = fav.text.charCodeAt(0);
        var codeHex = code.toString(16).toLowerCase();
        while (codeHex.length < 4) {
            codeHex = "0" + codeHex;
        }
        var like = "&#x" + codeHex + ";";

        if (like == "&#xf08a;")
            fav.text = String.fromCharCode(0xf004) + String.fromCharCode(0x0a) + "Like" // LIKE
        else
            fav.text = String.fromCharCode(0xf08a) + String.fromCharCode(0x0a) + "Like" // DISLIKE
            //String.fromCharCode(0xf004)
            // XML "&#xf08a;&#x0a;Like"

        //fav.text = String.fromCharCode(0xf004) + String.fromCharCode(0x0a) + "Like"
        //fav.text = "&#xf095;&#x0a;Like"

        //1716823698346138;
        var id = gotData.id;

        var url = "http://mustgo.com.mx/api/likes/user/" + userID + "/negocio/" + id;
        http.getJSON(url).then((res) => {
            console.log(res.liked);
        }, function(err) {
            console.log("Error getting like info: " + err);
        });
    } else {
        dialogs.alert({
            title: title.text,
            message: "Para poder guardarnos en tus favoritos, por favor inicia sesión en las redes sociales",
            okButtonText: "Ok"
        }).then(function() {
            console.log("Dialog closed!");
        });
    }
}
exports.bangThis = bangThis;

// GOOGLE MAPS

function onMapReady(args) {
    var mapView = args.object;

    coordx = appSettings.getNumber("coordx");
    coordy = appSettings.getNumber("coordy");

    console.log("Setting a marker...");
    var marker = new mapsModule.Marker();
    marker.position = mapsModule.Position.positionFromLatLng(coordx, coordy);
    marker.userData = { index: 1 };
    mapView.addMarker(marker);

    mapView.settings.scrollGesturesEnabled = false;
    mapView.settings.zoomGesturesEnabled = false;
    mapView.settings.mapToolbarEnabled = true;
}

function onMarkerSelect(args) {
    console.log("Clicked on " + args.marker.title);
}

function onCameraChanged(args) {
    console.log("Camera changed: " + JSON.stringify(args.camera));
}

exports.onMapReady = onMapReady;
exports.onMarkerSelect = onMarkerSelect;
exports.onCameraChanged = onCameraChanged;


// BOTTOM DRAWER

exports.toggleDrawer = function() {
    if (appSettings.getString("username") == undefined && appSettings.getString("profileimage") == undefined ||
        appSettings.getString("username") == null && appSettings.getString("profileimage") == null) {
        snackbar.action({
            actionText: "Iniciar Sesión",
            actionTextColor: '#ff4081', // Optional, Android only
            snackText: "Debes Iniciar Sesión Primero",
            hideDelay: 3500,
            actionClickFunction: frameModule.topmost().navigate("/views/login/login")
        }).then((args) => {
            if (args.command === "Action") {
                this.set('jsonResult', JSON.stringify(args));
            } else {
                this.set('jsonResult', JSON.stringify(args));
            }
        });
    } else drawer.toggleDrawerState();
}


// GALLERY

exports.openGallery = function() {
    // Add to array and pass to showViewer 
    photoViewer.showViewer(myImages);
}

exports.addComment = function() {
    var year = new Date().getFullYear();
    var month = new Date().getMonth();
    month = month + 1;
    var day = new Date().getDate();

    firebase.push(
        '/comments/' + gotData.id, {
            'username': appSettings.getString("username"),
            'comment': tvComment.text,
            'date': day + '/' + month + '/' + year,
            'profilepicture': appSettings.getString('profileimage')
        }
    ).then(
        function(result) {
            console.log("created key: " + result.key);
            queryFirebase()
            drawer.toggleDrawerState();
        }
    );
}

function queryFirebase() {
    firebase.query(
        onQueryEvent,
        '/comments/' + gotData.id, {
            // set this to true if you want to check if the value exists or just want the event to fire once
            // default false, so it listens continuously.
            // Only when true, this function will return the data in the promise as well!
            singleEvent: true,
            orderBy: {
                type: firebase.QueryOrderByType.CHILD,
                value: 'since'
            }
        }
    );
}

var onQueryEvent = function(result) {
    comments.splice(0, comments.length)
        // note that the query returns 1 match at a time
        // in the order specified in the query
    if (!result.error) {
        console.log("Event type: " + result.type);
        console.log("Key: " + result.key);
        console.log(JSON.stringify(result.value));

        if (result.value != null) {
            labelNoComment.visibility = "collapse"
            for (var id in result.value) {
                comments.push({ profilePicture: result.value[id].profilepicture, username: result.value[id].username, comment: result.value[id].comment, date: result.value[id].date })
            }
        } else {
            stackLayoutComments.height = 125;
        }
    }
    //comments = arraySort(comments, 'date')
};