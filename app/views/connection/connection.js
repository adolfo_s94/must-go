var frameModule = require("ui/frame");
var connectivity = require("connectivity");
var page;

exports.pageLoaded = function(args) {
    page = args.object;
    page.actionBarHidden = true;
}

connectivity.startMonitoring(function onConnectionTypeChanged(newConnectionType) {
    switch (newConnectionType) {
        case connectivity.connectionType.none:
            console.log("Connection type changed to none.");
            break;
        case connectivity.connectionType.wifi:
            frameModule.topmost().goBack();
            console.log("Connection type changed to WiFi.");
            break;
        case connectivity.connectionType.mobile:
            frameModule.topmost().goBack();
            console.log("Connection type changed to mobile.");
            break;
    }
});