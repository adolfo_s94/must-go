var Toast = require("nativescript-toast");
var frameModule = require("ui/frame");
var view = require("ui/core/view");
var app = require("application");
var appSettings = require("application-settings");
var topmost = require("ui/frame")
var http = require("tns-core-modules/http");
var color = require("color");
var platform = require("platform");
var geolocation = require("nativescript-geolocation");
var config = require("~/app.config").configMaps;
var firebase = require("nativescript-plugin-firebase");
var observableArray = require("data/observable-array");
var arraySort = require('array-sort');
var observableModule = require("data/observable");
var conf = require("~/app.config").config;
var utilsModule = require("tns-core-modules/utils/utils");
var platformModule = require("tns-core-modules/platform");
var colorModule = require("tns-core-modules/color");
var orientationModule = require("nativescript-screen-orientation");
var striptags = require('striptags');
var removeDuplicates = require('removeduplicates');
var dialog = require("nativescript-dialog");

var Color = colorModule.Color;

var page;
var drawer;
var menu;
var buttonLocation;
var header;
var username;
var profileimage;
var listviewlikes;
var socialNetwork;
var button;

//searchbar
var searchbar;

//LV
var array = new observableArray.ObservableArray();
var categories = new observableArray.ObservableArray();
var likes = new observableArray.ObservableArray();
var items = new observableArray.ObservableArray([]);
var sorts;

var viewModel = observableModule.fromObject({
    header: "A",
    myItems: array,
    myCategories: categories,
    myLikes: likes,
    items: items
});

// SB
var sb;
var sb2;
var sb3;

var option;
var option2;
var filterPrice;

//TabBar
var tabview;
var actionBar;


//Opciones Filtros Btns
var filterOpen;
var filterCC;
var filterParking;
var filterWifi;
var filterDomicilio;
var filterDiscapacitados;
var filterPets;
var filterLanguages;
var filterSmoke;
var filterEmergencias;
var filterPool;

var filterbtn;
var filterbtn1;
var filterbtn2;
var filterbtn3;
var filterbtn4;
var filterbtn5;
var filterbtn6;
var filterbtn7;
var filterbtn8;
var filterbtn9;
var filterbtn10;


// WIDTH SCREEN+

var width;

exports.pageLoaded = function(args) {
    orientationModule.setCurrentOrientation("portrait");

    page = args.object;
    page.actionBarHidden = true;

    actionBar = view.getViewById(page, "actionBar");

    width = platformModule.screen.mainScreen.widthPixels;

    if (app.android && platform.device.sdkVersion >= '21') {
        var window = app.android.startActivity.getWindow();
        window.setStatusBarColor(new color.Color('#c04228').android);
    }

    menu = view.getViewById(page, "menuDrawer");
    drawer = view.getViewById(page, "sideDrawer");

    width = utilsModule.layout.toDeviceIndependentPixels(width);
    width = (width * 3) / 4;

    drawer.drawerContentSize = width;
    menu.drawerContentSize = width;

    buttonLocation = view.getViewById(page, "localizacion");
    header = view.getViewById(page, "sLHeader");
    username = view.getViewById(page, "userProfile");
    profileimage = view.getViewById(page, "imgProfile");
    searchbar = view.getViewById(page, "searchbar");
    sb = view.getViewById(page, "sb");
    sb2 = view.getViewById(page, "sb2");
    sb3 = view.getViewById(page, "sb3");
    filterbtn = view.getViewById(page, "filterbtn")
    filterbtn1 = view.getViewById(page, "filterbtn1")
    filterbtn2 = view.getViewById(page, "filterbtn2")
    filterbtn3 = view.getViewById(page, "filterbtn3")
    filterbtn4 = view.getViewById(page, "filterbtn4")
    filterbtn5 = view.getViewById(page, "filterbtn5")
    filterbtn6 = view.getViewById(page, "filterbtn6")
    filterbtn7 = view.getViewById(page, "filterbtn7")
    filterbtn8 = view.getViewById(page, "filterbtn8")
    filterbtn9 = view.getViewById(page, "filterbtn9")
    filterbtn10 = view.getViewById(page, "filterbtn10")
    tabview = view.getViewById(page, "tab-view");
    listviewlikes = view.getViewById(page, "ListViewLikes");
    socialNetwork = view.getViewById(page, "socialNetwork");

    var catProfile = view.getViewById(page, "catProfile");

    getLocation(); // LOCATION

    sb.selectedIndex = 1;
    sb2.selectedIndex = 1;

    if (appSettings.getString("network") == "g") {
        socialNetwork.text = String.fromCharCode(0xf1a0);
        socialNetwork.backgroundColor = "#DD4B39";
    } else if (appSettings.getString("network") == null) {
        socialNetwork.visibility = "collapse";
    }

    searchbar.text = '';

    if (appSettings.getString("USERID") == null) {
        listviewlikes.backgroundImage = "~/images/nologin.png"
        catProfile.text = "INICIAR SESIÓN"
    }

    let rand = Math.floor((Math.random() * 6));
    //HEADER QUERY
    firebase.query(result => {
        if (result.key == rand)
            header.backgroundImage = result.value
    }, "/home/header", {
        orderBy: {
            type: firebase.QueryOrderByType.KEY
        }
    })

    if (appSettings.getString("profileimage") != null)
        profileimage.backgroundImage = appSettings.getString("profileimage");
    else
        profileimage.backgroundImage = "url('~/images/home/dummy.png')";
    if (appSettings.getString("username") != null)
        username.text = appSettings.getString("username");
    else
        username.text = "Bienvenido!";


    appSettings.setString("token", "");

    // Options
    filterOpen = 1;
    filterCC = 1;
    filterParking = 0;
    filterWifi = 0;
    filterDomicilio = 0;
    filterDiscapacitados = 0;
    filterPets = 0;
    filterLanguages = 0;
    filterSmoke = 0;
    filterEmergencias = 0;
    filterPool = 0;

    // ARRAYS DATABASE
    array.splice(0, array.length);
    categories.splice(0, categories.length);
    items.splice(0, items.length);

    sorts = [];

    page.bindingContext = viewModel;


    getLocationsSpinner();
    getCategories();
    buscar();
}


function getLocationsSpinner() {

    var arrayCiudad = [];

    http.getJSON("http://www.mustgo.com.mx/api/negocios/all").then((res) => {
        for (let i = 0; i < res.length; i++) {
            arrayCiudad.push(res[i].ciudad)
        }

        var uniqueArray = removeDuplicates.default(arrayCiudad);

        for (let i = 0; i < uniqueArray.length; i++) {
            viewModel.items.push(uniqueArray[i]);
        }
    }, function(e) {
        // error
    });
}

function getCategories() {
    return new Promise(function(resolve, reject) {
        try {
            viewModel.myCategories.splice(0, viewModel.myCategories.length);
            //CATEGORIES QUERY
            http.getJSON("http://www.mustgo.com.mx/api/categorias/all").then((res) => {
                for (var i = 0; i < res.length; i++) {
                    viewModel.myCategories.push({ category: res[i].nombre, img: res[i].headerApp });
                }
            }, function(err) {

            });
            resolve("great success");
        } catch (ex) {
            reject(ex);
        }
    });
}

exports.viewItem = function(args) {
    var itemIndex = args.index;
    var id = array.getItem(itemIndex).id;
    var imgProduct = array.getItem(itemIndex).imgProduct;
    var title = array.getItem(itemIndex).title;
    var path = array.getItem(itemIndex).path;
    var direccion = array.getItem(itemIndex).direccion;
    var description = array.getItem(itemIndex).description;
    var telefono = array.getItem(itemIndex).telefono;
    var rating = array.getItem(itemIndex).rating;
    var price = array.getItem(itemIndex).price;
    var fav = array.getItem(itemIndex).fav;
    var coordx = parseFloat(array.getItem(itemIndex).coordx);
    var coordy = parseFloat(array.getItem(itemIndex).coordy);
    var portada = array.getItem(itemIndex).portada;
    var cc = array.getItem(itemIndex).cc;
    var wifi = array.getItem(itemIndex).wifi;
    var estacionamiento = array.getItem(itemIndex).estacionamiento;
    var domicilio = array.getItem(itemIndex).domicilio;
    var discapacitados = array.getItem(itemIndex).discapacitados;
    var mascotas = array.getItem(itemIndex).mascotas;
    var idiomas = array.getItem(itemIndex).idiomas;
    var fumar = array.getItem(itemIndex).fumar;
    var emergencias = array.getItem(itemIndex).emergencias;
    var alberca = array.getItem(itemIndex).alberca;
    var facebookPub = array.getItem(itemIndex).facebookPub;
    var horarios = array.getItem(itemIndex).horarios;
    var visitas = array.getItem(itemIndex).visitas;

    appSettings.setNumber("coordx", coordx);
    appSettings.setNumber("coordy", coordy);

    var navigationOptions = {
        moduleName: '/views/item/item',
        context: {
            id,
            imgProduct,
            title,
            path,
            direccion,
            description,
            telefono,
            rating,
            price,
            fav,
            coordx,
            coordy,
            portada,
            cc,
            wifi,
            estacionamiento,
            domicilio,
            discapacitados,
            mascotas,
            idiomas,
            fumar,
            emergencias,
            alberca,
            facebookPub,
            horarios,
            visitas,
            itemIndex
        }
    }
    frameModule.topmost().navigate(navigationOptions);
};

exports.viewItemLikes = function(args) {
    var itemIndex = args.index;
    var id = viewModel.myLikes.getItem(itemIndex).id;
    var imgProduct = viewModel.myLikes.getItem(itemIndex).imgProduct;
    var title = viewModel.myLikes.getItem(itemIndex).title;
    var path = viewModel.myLikes.getItem(itemIndex).path;
    var direccion = array.getItem(itemIndex).direccion;
    var description = viewModel.myLikes.getItem(itemIndex).description;
    var telefono = viewModel.myLikes.getItem(itemIndex).telefono;
    var rating = viewModel.myLikes.getItem(itemIndex).rating;
    var price = viewModel.myLikes.getItem(itemIndex).price;
    var fav = viewModel.myLikes.getItem(itemIndex).fav;
    var coordx = parseFloat(viewModel.myLikes.getItem(itemIndex).coordx);
    var coordy = parseFloat(viewModel.myLikes.getItem(itemIndex).coordy);
    var portada = viewModel.myLikes.getItem(itemIndex).portada;
    var cc = viewModel.myLikes.getItem(itemIndex).cc;
    var wifi = viewModel.myLikes.getItem(itemIndex).wifi;
    var estacionamiento = viewModel.myLikes.getItem(itemIndex).estacionamiento;
    var domicilio = viewModel.myLikes.getItem(itemIndex).domicilio;
    var discapacitados = viewModel.myLikes.getItem(itemIndex).discapacitados;
    var mascotas = viewModel.myLikes.getItem(itemIndex).mascotas;
    var idiomas = viewModel.myLikes.getItem(itemIndex).idiomas;
    var fumar = viewModel.myLikes.getItem(itemIndex).fumar;
    var emergencias = viewModel.myLikes.getItem(itemIndex).emergencias;
    var alberca = viewModel.myLikes.getItem(itemIndex).alberca;
    var facebookPub = viewModel.myLikes.getItem(itemIndex).facebookPub;
    var horarios = viewModel.myLikes.getItem(itemIndex).horarios;
    var visitas = viewModel.myLikes.getItem(itemIndex).visitas;

    appSettings.setNumber("coordx", coordx);
    appSettings.setNumber("coordy", coordy);

    var navigationOptions = {
        moduleName: '/views/item/item',
        context: {
            id,
            imgProduct,
            title,
            path,
            direccion,
            description,
            telefono,
            rating,
            price,
            fav,
            coordx,
            coordy,
            portada,
            cc,
            wifi,
            estacionamiento,
            domicilio,
            discapacitados,
            mascotas,
            idiomas,
            fumar,
            emergencias,
            alberca,
            facebookPub,
            horarios,
            visitas,
            itemIndex
        }
    }
    frameModule.topmost().navigate(navigationOptions);
};

exports.gridViewItemTap = function(args) {
    var itemIndex = args.index;
    var title = viewModel.myCategories.getItem(itemIndex).category;
    //var header = result.value.header

    var navigationOptions = {
        moduleName: '/views/categoria/categoria',
        context: {
            title: title
        }
    }
    frameModule.topmost().navigate(navigationOptions);
}

function getLocation() {
    var location = geolocation.getCurrentLocation({ desiredAccuracy: 3, updateDistance: 10, maximumAge: 20000, timeout: 20000 }).
    then(function(loc) {
        if (loc) {
            console.log("Current location is: " + loc.latitude + ", " + loc.longitude);
            appSettings.setNumber("currentLocationLat", loc.latitude);
            appSettings.setNumber("currentLocationLong", loc.longitude);
            // Get current direccion in user's info   
            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + loc.latitude + "," + loc.longitude + "&key=" + config.GEOCODING_API_KEY;
            http.getJSON(url).then((res) => {
                location = res.results[2].formatted_address;
                buttonLocation.text = String.fromCharCode(0xf124) + " " + location;
                viewModel.items.push(location);
            }, function(err) {

            });
        }
    }, function(e) {
        console.log("Error: " + e.message);
        buttonLocation.text = String.fromCharCode(0xf124) + " " + "México";
        viewModel.items.push("México");
        appSettings.setString("token", "México");
    });
}


function getDistance(lat2, lon2) {

    var lat1 = appSettings.getNumber("currentLocationLat");
    var lon1 = appSettings.getNumber("currentLocationLong");

    var p = 0.017453292519943295; // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p)) / 2;

    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

exports.toggleDrawer = function() {
    drawer.toggleDrawerState();
};

exports.toggleMenu = function() {
    menu.toggleDrawerState();
}

exports.banner = function(args) {
    button = args.object;
    var title = button.text;

    var navigationOptions = {
        moduleName: '/views/banners/banners',
        context: {
            title: title
        }
    }
    frameModule.topmost().navigate(navigationOptions);
}

exports.filtros = function(args) {
    button = args.object;

    tabview.selectedIndex = 1;

    var choice;

    switch (button.text) {
        case "Recomendado":
            sb2.selectedIndex = 2;
            choice = "recomendado";
            break;
        case "Lo más popular":
            sb.selectedIndex = 0;
            choice = "visitas";
            break;
        case "Nuevo y de Moda":
            choice = "rating";
            break;
        default:
            choice = "recomendado"
            break;
    }

    viewModel.myItems.splice(0, viewModel.myItems.length);

    var res = arraySort(sorts, choice).reverse();

    for (let i = 0; i < sorts.length; i++) {
        var id = res[i].id;
        var name = res[i].title;
        var path = res[i].path;
        var description = res[i].description;
        var phone = res[i].telefono;
        var rating = res[i].rating;
        var price = res[i].price;
        var iconList = res[i].imgProduct;
        var portada = res[i].portada;
        var distance = getDistance(res[i].coordx, res[i].coordy);
        distance = distance.toFixed(1);
        var facebookPub = res[i].facebookPub;
        var horarios = res[i].horarios;
        var visitas = res[i].visitas;
        var direccion = res[i].direccion;

        var estacionamiento = res[i].estacionamiento;
        var wifi = res[i].wifi;
        var cc = res[i].cc;
        var domicilio = res[i].domicilio;
        var discapacitados = res[i].discapacitados;
        var mascotas = res[i].mascotas;
        var idiomas = res[i].idiomas;
        var fumar = res[i].fumar;
        var emergencias = res[i].emergencias;
        var alberca = res[i].alberca;

        var promocion = res[i].promocion;
        var recomendado = res[i].recomendado;

        var fav = String.fromCharCode(0xf164);
        if (res[i].recomendado == 0) {
            fav = String.fromCharCode(0xf087)
        }

        viewModel.myItems.push({
            id: id,
            imgProduct: iconList,
            title: name,
            path: path,
            description: description,
            telefono: phone,
            distance: distance,
            rating: rating,
            price: price,
            fav: fav,
            coordx: res[i].coordx,
            coordy: res[i].coordy,
            portada: portada,
            cc: cc,
            wifi: wifi,
            estacionamiento: estacionamiento,
            facebookPub: facebookPub,
            horarios: horarios,
            visitas: visitas,
            direccion: direccion,
            promocion: promocion,
            recomendado: recomendado,
            domicilio: domicilio,
            discapacitados: discapacitados,
            mascotas: mascotas,
            idiomas: idiomas,
            fumar: fumar,
            emergencias: emergencias,
            alberca: alberca
        });
    }
}


exports.getFilters = function(args) {

    switch (sb.selectedIndex) {
        case 0:
            option = "visitas";
            break;
        case 1:
            option = "distance";
            break;
        case 2:
            option = "rating";
            break;
    }
    switch (sb2.selectedIndex) {
        case 0:
            option2 = "promocion";
            break;
        case 1:
            option2 = "title";
            break;
        case 2:
            option2 = "recomendado";
            break;
    }
    switch (sb3.selectedIndex) {
        case 0:
            filterPrice = "$";
            break;
        case 1:
            filterPrice = "$$";
            break;
        case 2:
            filterPrice = "$$$";
            break;
    }

    viewModel.myItems.splice(0, viewModel.myItems.length);

    var res = arraySort(sorts, option, option2, filterPrice);

    for (let i = 0; i < sorts.length; i++) {
        if (res[i].price == filterPrice) {
            if (res[i].cc == filterCC || res[i].estacionamiento == filterParking || res[i].wifi == filterWifi ||
                res[i].domicilio == filterDomicilio || res[i].discapacitados == filterDiscapacitados || res[i].mascotas == filterPets ||
                res[i].idiomas == filterLanguages || res[i].fumar == filterSmoke || res[i].emergencias == filterEmergencias ||
                res[i].alberca == filterPool) {

                var id = res[i].id;
                var name = res[i].title;
                var path = res[i].path;
                var description = res[i].description;
                var phone = res[i].telefono;
                var rating = res[i].rating;
                var price = res[i].price;
                var iconList = res[i].imgProduct;
                var portada = res[i].portada;
                var distance = getDistance(res[i].coordx, res[i].coordy);
                distance = distance.toFixed(1);
                var facebookPub = res[i].facebookPub;
                var horarios = res[i].horarios;
                var visitas = res[i].visitas;
                var direccion = res[i].direccion;

                var estacionamiento = res[i].estacionamiento;
                var wifi = res[i].wifi;
                var cc = res[i].cc;
                var domicilio = res[i].domicilio;
                var discapacitados = res[i].discapacitados;
                var mascotas = res[i].mascotas;
                var idiomas = res[i].idiomas;
                var fumar = res[i].fumar;
                var emergencias = res[i].emergencias;
                var alberca = res[i].alberca;

                var promocion = res[i].promocion;
                var recomendado = res[i].recomendado;

                var fav = String.fromCharCode(0xf164);
                if (res[i].recomendado == 0) {
                    fav = String.fromCharCode(0xf087)
                }


                viewModel.myItems.push({
                    id: id,
                    imgProduct: iconList,
                    title: name,
                    path: path,
                    description: description,
                    telefono: phone,
                    distance: distance,
                    rating: rating,
                    price: price,
                    fav: fav,
                    coordx: res[i].coordx,
                    coordy: res[i].coordy,
                    portada: portada,
                    cc: cc,
                    wifi: wifi,
                    estacionamiento: estacionamiento,
                    facebookPub: facebookPub,
                    horarios: horarios,
                    visitas: visitas,
                    direccion: direccion,
                    promocion: promocion,
                    recomendado: recomendado,
                    domicilio: domicilio,
                    discapacitados: discapacitados,
                    mascotas: mascotas,
                    idiomas: idiomas,
                    fumar: fumar,
                    emergencias: emergencias,
                    alberca: alberca
                });
            }
        }
    }
}

exports.setFilter = function(args) { // FILTROS
    var btn = args.object;

    if (btn.backgroundColor == "#F87252") {
        btn.backgroundColor = "white";
        btn.color = "#F87252";
        switch (btn.text) {
            case "Abierto ahora":
                filterOpen = 0;
                break;
            case "Tarjeta de Crédito":
                filterCC = 0;
                break;
            case "Estacionamiento":
                filterParking = 0;
                break;
            case "WiFi":
                filterWifi = 0;
                break;
            case "Servicio a Domicilio":
                filterDomicilio = 0;
                break;
            case "Acceso a Discapacitados":
                filterDiscapacitados = 0;
                break;
            case "Mascotas permitidas":
                filterPets = 0;
                break;
            case "Otros Idiomas":
                filterLanguages = 0;
                break;
            case "Área de fumar":
                filterSmoke = 0;
                break;
            case "Kit de Emergencias":
                filterEmergencias = 0;
                break;
            case "Alberca":
                filterPool = 0;
                break;
        }
    } else {
        btn.backgroundColor = "#F87252";
        btn.color = "white";
        switch (btn.text) {
            case "Abierto ahora":
                filterOpen = 1;
                break;
            case "Tarjeta de Crédito":
                filterCC = 1;
                break;
            case "Estacionamiento":
                filterParking = 1;
                break;
            case "WiFi":
                filterWifi = 1;
                break;
            case "Servicio a Domicilio":
                filterDomicilio = 1;
                break;
            case "Acceso a Discapacitados":
                filterDiscapacitados = 1;
                break;
            case "Mascotas permitidas":
                filterPets = 1;
                break;
            case "Otros Idiomas":
                filterLanguages = 1;
                break;
            case "Área de fumar":
                filterSmoke = 1;
                break;
            case "Kit de Emergencias":
                filterEmergencias = 1;
                break;
            case "Alberca":
                filterPool = 1;
                break;
        }
    }
}

function clean() {
    filterOpen = 1;
    filterCC = 1;
    filterParking = 0;
    filterWifi = 0;
    filterDomicilio = 0;
    filterDiscapacitados = 0;
    filterPets = 0;
    filterLanguages = 0;
    filterSmoke = 0;
    filterEmergencias = 0;
    filterPool = 0;

    sb.selectedIndex = 1;
    sb2.selectedIndex = 1;

    filterbtn.backgroundColor = "#F87252";
    filterbtn.color = "white";
    filterbtn1.backgroundColor = "#F87252";
    filterbtn1.color = "white";
    filterbtn2.backgroundColor = "white";
    filterbtn2.color = "#F87252";
    filterbtn3.backgroundColor = "white";
    filterbtn3.color = "#F87252";
    filterbtn4.backgroundColor = "white";
    filterbtn4.color = "#F87252";
    filterbtn5.backgroundColor = "white";
    filterbtn5.color = "#F87252";
    filterbtn6.backgroundColor = "white";
    filterbtn6.color = "#F87252";
    filterbtn7.backgroundColor = "white";
    filterbtn7.color = "#F87252";
    filterbtn8.backgroundColor = "white";
    filterbtn8.color = "#F87252";
    filterbtn9.backgroundColor = "white";
    filterbtn9.color = "#F87252";
    filterbtn10.backgroundColor = "white";
    filterbtn10.color = "#F87252";

    searchbar.text = '';

    viewModel.myItems.splice(0, array.length);

    http.getJSON("http://mustgo.com.mx/api/negocios/all").then((res) => {
        for (var i = 0; i < res.length; i++) {
            var id = res[i].id;
            var name = res[i].nombre;
            var path = res[i].dirCategoria;
            var description = striptags(res[i].descripcion, [], '');
            if (description.length > 180)
                description = description.substring(0, 180).concat("...");
            var phone = res[i].telefono;
            var rating = "";
            for (var j = 1; j <= res[i].calificacion; j++)
                rating = rating + String.fromCharCode(0xf005);
            var price = "";
            for (var k = 1; k <= res[i].precio; k++)
                price = price + '$';
            var iconList = res[i].logo;
            var portada = res[i].portada;
            var distance = getDistance(res[i].cordx, res[i].cordy);
            distance = distance.toFixed(1);
            var direccion = res[i].direccion;

            var estacionamiento = res[i].estacionamiento;
            var wifi = res[i].WIFI;
            var cc = res[i].CC;
            var domicilio = res[i].domicilio;
            var discapacitados = res[i].discapacitados;
            var mascotas = res[i].mascotas;
            var idiomas = res[i].idiomas;
            var fumar = res[i].fumar;
            var emergencias = res[i].emergencias;
            var alberca = res[i].alberca;

            var promocion = res[i].promocion;
            var recomendado = res[i].recomendado;

            var coordx = res[i].cordx;
            var coordy = res[i].cordy;

            var facebookPub = res[i].facebookPub;
            var horarios = res[i].horarios;
            var visitas = res[i].visitas;

            var fav = String.fromCharCode(0xf164);
            if (res[i].recomendado == 0) {
                fav = String.fromCharCode(0xf087);
            }

            viewModel.myItems.push({
                id: id,
                imgProduct: iconList,
                title: name,
                path: path,
                description: description,
                telefono: phone,
                distance: distance,
                rating: rating,
                price: price,
                fav: fav,
                coordx: coordx,
                coordy: coordy,
                portada: portada,
                cc: cc,
                wifi: wifi,
                estacionamiento: estacionamiento,
                facebookPub: facebookPub,
                horarios: horarios,
                visitas: visitas,
                direccion: direccion,
                promocion: promocion,
                recomendado: recomendado,
                domicilio: domicilio,
                discapacitados: discapacitados,
                mascotas: mascotas,
                idiomas: idiomas,
                fumar: fumar,
                emergencias: emergencias,
                alberca: alberca
            });
        }
    }, function(err) {
        console.log(err);
    });
}

exports.cleanFilters = function() {
    sb.selectedIndex = 0;
    sb2.selectedIndex = 0;
    sb3.selectedIndex = 0;

    clean();
}

exports.search = function(args) {
    var search = args.object;
    if (search.text != '') {
        searchbar.text = search.text;
        tabview.selectedIndex = 1;
    } else {
        tabview.selectedIndex = 1;
    }
    buscar();

}


function buscar() { // BUSCAR NOMBRE Y CATEGORIA

    return new Promise(function(resolve, reject) {
        try {

            var txtsearch = searchbar.text;
            var txtaddress = appSettings.getString("token");

            txtsearch = txtsearch.toLowerCase();
            txtaddress = txtaddress.toLowerCase();

            var userID = appSettings.getString("USERID");

            array.splice(0, array.length);
            sorts.splice(0, sorts.length);
            viewModel.myItems.splice(0, viewModel.myItems.length);
            viewModel.myLikes.splice(0, viewModel.myLikes.length);


            if (userID != undefined) {
                var url = "http://mustgo.com.mx/api/likes/user/" + userID;
                console.log("URL LIKES: " + url)
                http.getJSON(url).then((res) => {
                    for (var i = 0; i < res.length; i++) {
                        var id = res[i].negocio.id;
                        var name = res[i].negocio.nombre;
                        var path = res[i].negocio.dirCategoria;
                        var description = res[i].negocio.descripcion;
                        var phone = res[i].negocio.telefono;
                        var rating = "";
                        for (var j = 1; j <= res[i].negocio.calificacion; j++)
                            rating = rating + String.fromCharCode(0xf005);
                        var price = "";
                        for (var k = 1; k <= res[i].negocio.precio; k++)
                            price = price + '$';
                        var iconList = res[i].negocio.logo;
                        var portada = res[i].negocio.portada;
                        var distance = getDistance(res[i].negocio.cordx, res[i].negocio.cordy);
                        distance = distance.toFixed(1);
                        var direccion = res[i].negocio.direccion;

                        var estacionamiento = res[i].negocio.estacionamiento;
                        var wifi = res[i].negocio.WIFI;
                        var cc = res[i].negocio.CC;
                        var domicilio = res[i].negocio.domicilio;
                        var discapacitados = res[i].negocio.discapacitados;
                        var mascotas = res[i].negocio.mascotas;
                        var idiomas = res[i].negocio.idiomas;
                        var fumar = res[i].negocio.fumar;
                        var emergencias = res[i].negocio.emergencias;
                        var alberca = res[i].negocio.alberca;

                        var promocion = res[i].negocio.promocion;
                        var recomendado = res[i].negocio.recomendado;

                        var coordx = res[i].negocio.cordx;
                        var coordy = res[i].negocio.cordy;

                        var facebookPub = res[i].negocio.facebookPub;
                        var horarios = res[i].negocio.horarios;
                        var visitas = res[i].negocio.visitas;

                        viewModel.myLikes.push({
                            id: id,
                            imgProduct: iconList,
                            title: name,
                            path: path,
                            description: description,
                            telefono: phone,
                            distance: distance,
                            rating: rating,
                            price: price,
                            fav: String.fromCharCode(0xf004),
                            coordx: coordx,
                            coordy: coordy,
                            portada: portada,
                            cc: cc,
                            wifi: wifi,
                            estacionamiento: estacionamiento,
                            facebookPub: facebookPub,
                            horarios: horarios,
                            visitas: visitas,
                            direccion: direccion,
                            promocion: promocion,
                            recomendado: recomendado,
                            domicilio: domicilio,
                            discapacitados: discapacitados,
                            mascotas: mascotas,
                            idiomas: idiomas,
                            fumar: fumar,
                            emergencias: emergencias,
                            alberca: alberca
                        });
                    }
                }, function(err) {
                    console.log("Error getting like info: " + err);
                });
            }
            http.getJSON("http://mustgo.com.mx/api/negocios/all").then((res) => {
                for (var i = 0; i < res.length; i++) {
                    var name = res[i].nombre;
                    name = name.toLowerCase();
                    var path = res[i].dirCategoria;
                    path = path.toLowerCase();
                    var ciudad = res[i].ciudad;
                    ciudad = ciudad.toLowerCase();
                    var hashtags = res[i].palabras_clave;
                    hashtags = hashtags.toLowerCase();
                    if ((name.includes(txtsearch) || path.includes(txtsearch) || hashtags.includes(txtsearch)) && ciudad.includes(txtaddress)) {
                        var id = res[i].id;
                        name = res[i].nombre;
                        path = res[i].dirCategoria;
                        var description = striptags(res[i].descripcion, [], '');
                        if (description.length > 180)
                            description = description.substring(0, 180).concat("...");
                        var phone = res[i].telefono;
                        var rating = "";
                        for (var j = 1; j <= res[i].calificacion; j++)
                            rating = rating + String.fromCharCode(0xf005);
                        var price = "";
                        for (var k = 1; k <= res[i].precio; k++)
                            price = price + '$';
                        var iconList = res[i].logo;
                        var portada = res[i].portada;
                        var distance = getDistance(res[i].cordx, res[i].cordy);
                        distance = distance.toFixed(1);
                        var direccion = res[i].direccion;

                        var estacionamiento = res[i].estacionamiento;
                        var wifi = res[i].WIFI;
                        var cc = res[i].CC;
                        var domicilio = res[i].domicilio;
                        var discapacitados = res[i].discapacitados;
                        var mascotas = res[i].mascotas;
                        var idiomas = res[i].idiomas;
                        var fumar = res[i].fumar;
                        var emergencias = res[i].emergencias;
                        var alberca = res[i].alberca;

                        var promocion = res[i].promocion;
                        var recomendado = res[i].recomendado;

                        var coordx = res[i].cordx;
                        var coordy = res[i].cordy;

                        var facebookPub = res[i].facebookPub;
                        var horarios = res[i].horarios;
                        var visitas = res[i].visitas;


                        var fav = String.fromCharCode(0xf164);
                        if (res[i].recomendado == 0) {
                            fav = String.fromCharCode(0xf087)
                        }

                        viewModel.myItems.push({
                            id: id,
                            imgProduct: iconList,
                            title: name,
                            path: path,
                            description: description,
                            telefono: phone,
                            distance: distance,
                            rating: rating,
                            price: price,
                            fav: fav,
                            coordx: coordx,
                            coordy: coordy,
                            portada: portada,
                            cc: cc,
                            wifi: wifi,
                            estacionamiento: estacionamiento,
                            facebookPub: facebookPub,
                            horarios: horarios,
                            visitas: visitas,
                            direccion: direccion,
                            promocion: promocion,
                            recomendado: recomendado,
                            domicilio: domicilio,
                            discapacitados: discapacitados,
                            mascotas: mascotas,
                            idiomas: idiomas,
                            fumar: fumar,
                            emergencias: emergencias,
                            alberca: alberca
                        });
                        sorts.push({
                            id: id,
                            imgProduct: iconList,
                            title: name,
                            path: path,
                            description: description,
                            telefono: phone,
                            distance: distance,
                            rating: rating,
                            price: price,
                            fav: fav,
                            coordx: coordx,
                            coordy: coordy,
                            portada: portada,
                            cc: cc,
                            wifi: wifi,
                            estacionamiento: estacionamiento,
                            facebookPub: facebookPub,
                            horarios: horarios,
                            visitas: visitas,
                            direccion: direccion,
                            promocion: promocion,
                            recomendado: recomendado,
                            domicilio: domicilio,
                            discapacitados: discapacitados,
                            mascotas: mascotas,
                            idiomas: idiomas,
                            fumar: fumar,
                            emergencias: emergencias,
                            alberca: alberca
                        });
                    }

                }
            }, function(err) {
                console.log(err);
            });
            resolve("great success");
        } catch (ex) {
            reject(ex);
        }
    });
}

exports.openURL = function(args) {
    button = args.object;

    var code = button.text.charCodeAt(0);
    var codeHex = code.toString(16).toLowerCase();
    while (codeHex.length < 4) {
        codeHex = "0" + codeHex;
    }
    var icon = "&#x" + codeHex + ";";

    switch (icon) {
        case "&#xf187;":
            utilsModule.openUrl("http://mustgo.com.mx/contacto/paquetes_precios");
            break;
        case "&#xf090;":
            utilsModule.openUrl("http://mustgo.com.mx/unete/");
            break;
        case "&#xf02d;":
            utilsModule.openUrl("http://mustgo.com.mx/blog/");
            break;
        case "&#xf128;":
            utilsModule.openUrl("http://mustgo.com.mx/FAQ");
            break;
        case "&#xf0e0;":
            utilsModule.openUrl("http://mustgo.com.mx/contacto/");
            break;
    }
}

exports.goLogin = function() {
    if (appSettings.getString("USERID") == null) {
        var navigationOptions = {
            moduleName: '/views/login/login',
            clearHistory: true
        }
        frameModule.topmost().navigate(navigationOptions);
    }
}

exports.onItemLoading = function(args) {
    if (app.ios) {
        var newcolor = (new Color("#00FFFFFF")).ios;
        args.ios.backgroundView.backgroundColor = newcolor;
    }
}

exports.logout = function() {
    dialog.show({
        title: "Must Go",
        message: "¿Cerrar sesión?",
        okButtonText: "Ok",
        cancelButtonText: "Cancel"
    }).then(function(result) {
        // result argument is boolean
        if (result) {
            firebase.logout();

            Toast.makeText("Cerrando Sesión..").show();

            appSettings.remove("USERID");
            appSettings.remove("username");
            appSettings.remove("profileimage");
            appSettings.remove("network");

            var navigationOptions = {
                moduleName: '/views/login/login',
                clearHistory: true
            }
            frameModule.topmost().navigate(navigationOptions);
        }
    });
}


exports.goToCategories = function() {
    tabview.selectedIndex = 2;
}

exports.dropDownSelectedIndexChanged = function(args) {
    if (args.newIndex != null || args.newIndex != undefined)
        appSettings.setString("token", viewModel.items.getItem(args.newIndex));
    buscar();
}


exports.onSelectedIndexChanged = function(args) {
    if (args.newIndex == 0 && actionBar != undefined)
        actionBar.title = "Inicio"
    else if (args.newIndex == 1)
        actionBar.title = "Buscar"
    else if (args.newIndex == 2)
        actionBar.title = "Categorias"
    else if (args.newIndex == 3)
        actionBar.title = "Perfil"


    if (args.newIndex == 1 && appSettings.getString("token") == "")
        Toast.makeText("Todos").show();
}