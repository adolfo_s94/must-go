var Toast = require("nativescript-toast");
var frameModule = require("ui/frame");
var appSettings = require("application-settings")
var imageSource = require("tns-core-modules/image-source");
var http = require("tns-core-modules/http");
var config = require("~/app.config").config;
var http = require("tns-core-modules/http");
var firebase = require("nativescript-plugin-firebase");
var FeedbackPlugin = require("nativescript-feedback");
var color = require("color");
var LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
var feedback = new FeedbackPlugin.Feedback();
var loader = new LoadingIndicator();
var page;

var logged;

exports.pageLoaded = function(args) {
    page = args.object;
    page.actionBarHidden = true;

    if (appSettings.getString("username") != null || appSettings.getString("username") != undefined) {
        var nagiationEntry = {
            moduleName: "/views/home/home",
            clearHistory: true
        };
        frameModule.topmost().navigate(nagiationEntry);
    }
};

exports.signIn = function() {
    feedback.show({
        message: "Bienvenido a Must Go",
        backgroundColor: new color.Color("#FFC542")
    });
    var nagiationEntry = {
        moduleName: "/views/home/home",
        clearHistory: true
    };
    frameModule.topmost().navigate(nagiationEntry);
};

exports.gsbTap = function() {
    getGoogleInfo();
}

exports.onLogin = function() {
    getFacebookInfo();
}

function getFacebookInfo() {
    logged = "f";
    Toast.makeText("Facebook: Iniciando Sesión..").show();
    firebase.login({
        type: firebase.LoginType.FACEBOOK,
        // Optional
        facebookOptions: {
            // defaults to ['public_profile', 'email']
            scope: ['public_profile', 'email']
        }
    }).then(
        function(result) {
            feedback.show({
                message: "Logged into Facebook",
                backgroundColor: new color.Color("#2E68B0"),
                icon: "facebook"
            });
            appSettings.setString("USERID", result.uid);
            appSettings.setString("username", result.name);
            appSettings.setString("profileimage", result.profileImageURL);
            appSettings.setString("network", logged);

            var nagiationEntry = {
                moduleName: "/views/home/home",
                clearHistory: true,
                animated: true,
                transition: {
                    name: "slide",
                    duration: 1.5,
                    curve: "easeIn"
                }
            };
            frameModule.topmost().navigate(nagiationEntry);
        },
        function(errorMessage) {
            console.log(errorMessage);
        }
    );
}

function getGoogleInfo() {
    logged = "g";
    Toast.makeText("Google: Iniciando Sesión..").show();
    firebase.login({
        type: firebase.LoginType.GOOGLE
            // Optional 
    }).then(
        function(result) {
            feedback.show({
                message: "Logged into Google",
                backgroundColor: new color.Color("#DD4B39"),
                icon: "google"
            });
            appSettings.setString("USERID", result.uid);
            appSettings.setString("username", result.name);
            appSettings.setString("profileimage", result.profileImageURL);
            appSettings.setString("network", logged);

            var nagiationEntry = {
                moduleName: "/views/home/home",
                clearHistory: true,
                animated: true,
                transition: {
                    name: "slide",
                    duration: 1.5,
                    curve: "easeIn"
                }
            };
            frameModule.topmost().navigate(nagiationEntry);
        },
        function(errorMessage) {
            console.log(errorMessage);
        }
    );
}