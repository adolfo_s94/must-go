var app = require("application");
var color = require("color");
var platform = require("platform");
var view = require("ui/core/view");
var frameModule = require("ui/frame");
var firebase = require("nativescript-plugin-firebase");
var observableArray = require("data/observable-array");
var http = require("tns-core-modules/http");
var appSettings = require("application-settings");
var removeDuplicates = require('removeduplicates');
var subcategories = new observableArray.ObservableArray();
var SnackBar = require("nativescript-snackbar");
var snackbar = new SnackBar.SnackBar();

var page;
var header;
var categoryText;
var gotData;


var catID;

exports.pageLoaded = function(args) {
    page = args.object;
    gotData = page.navigationContext;

    subcategories.splice(0, subcategories.length);

    header = view.getViewById(page, "sLHeader");
    categoryText = view.getViewById(page, "categoryText");

    categoryText.text = gotData.title;

    //CATEGORIES QUERY
    http.getJSON("http://www.mustgo.com.mx/api/categorias/all").then((res) => {
        for (var i = 0; i < res.length; i++) {
            if (res[i].nombre == gotData.title) {
                header.backgroundImage = res[i].headerApp;
                catID = res[i].id;

                res[i].subcategorias = removeDuplicates.default(res[i].subcategorias, 'nombre');

                for (var j = 0; j < res[i].subcategorias.length; j++)
                    subcategories.push({ subcategory: res[i].subcategorias[j].nombre, id: res[i].id, idsubcategory: res[i].subcategorias[j].id_subcategoria });
            }
        }
    }, function(err) {
        console.log("Error getting user info: " + err);
    });

    page.bindingContext = { myItems: subcategories };
}

exports.viewAll = function() {
    var categoria = gotData.title;
    var id = catID;
    var all = true;

    var navigationOptions = {
        moduleName: '/views/subcategoria/subcategoria',
        context: {
            categoria,
            id,
            all
        }
    }
    frameModule.topmost().navigate(navigationOptions);
}

exports.viewItem = function(args) {
    var index = args.index;
    var categoria = gotData.title;
    var subcategoria = subcategories.getItem(index).subcategory;
    var idsubcategory = subcategories.getItem(index).idsubcategory;
    var all = false;

    var navigationOptions = {
        moduleName: '/views/subcategoria/subcategoria',
        context: {
            categoria,
            subcategoria,
            idsubcategory,
            all
        }
    }
    frameModule.topmost().navigate(navigationOptions);
}


exports.onNavBtnTap = function() {
    console.log("Navigation button tapped!");
    frameModule.topmost().goBack();
}