var app = require("application");
var view = require("ui/core/view");
var frameModule = require("ui/frame");
var observableArray = require("data/observable-array");
var http = require("tns-core-modules/http");
var striptags = require('striptags');
var appSettings = require("application-settings");
var banner = new observableArray.ObservableArray();

var page;
var gotData;

exports.pageLoaded = function(args) {
    page = args.object;
    gotData = page.navigationContext;

    var actionBar = view.getViewById(page, "actionBar");

    actionBar.title = gotData.title;

    banner.splice(0, banner.length);

    http.getJSON("http://mustgo.com.mx/api/banners/all").then((res) => {
        for (var i = 0; i < res.length; i++) {
            banner.push({
                category: res[i].nombre_negocio,
                imgBanner: res[i].nombre,
                id: res[i].id_negocio
            })
        }
    }, function(err) {
        console.log("Error getting banners info: " + err);
    });

    page.bindingContext = { myBanners: banner };
}

exports.viewItem = function(args) {
    var itemIndex = args.index;
    var id = banner.getItem(itemIndex).id;

    http.getJSON("http://www.mustgo.com.mx/api/negocios/getById/" + id).then((result) => {
        var title = result.nombre;
        var path = result.dirCategoria;
        var description = striptags(result.descripcion, [], '');
        var telefono = result.telefono;
        var rating = "";
        for (var j = 1; j <= result.calificacion; j++)
            rating = rating + String.fromCharCode(0xf005);
        var price = "";
        for (var k = 1; k <= result.precio; k++)
            price = price + '$';
        var imgProduct = result.logo;
        var portada = result.portada;
        var direccion = result.direccion;

        var estacionamiento = result.estacionamiento;
        var wifi = result.WIFI;
        var cc = result.CC;
        var domicilio = result.domicilio;
        var discapacitados = result.discapacitados;
        var mascotas = result.mascotas;
        var idiomas = result.idiomas;
        var fumar = result.fumar;
        var emergencias = result.emergencias;
        var alberca = result.alberca;

        var coordx = result.cordx;
        var coordy = result.cordy;

        var facebookPub = result.facebookPub;
        var horarios = result.horarios;
        var visitas = result.visitas;

        var navigationOptions = {
            moduleName: '/views/item/item',
            context: {
                id,
                imgProduct,
                title,
                path,
                direccion,
                description,
                telefono,
                rating,
                price,
                coordx,
                coordy,
                portada,
                cc,
                wifi,
                estacionamiento,
                domicilio,
                discapacitados,
                mascotas,
                idiomas,
                fumar,
                emergencias,
                alberca,
                facebookPub,
                horarios,
                visitas,
                itemIndex
            }
        }
        frameModule.topmost().navigate(navigationOptions);
    }, function(err) {
        console.log("Error getting banners info: " + err);
    });
}


exports.onNavBtnTap = function() {
    console.log("Navigation button tapped!");
    frameModule.topmost().goBack();
}