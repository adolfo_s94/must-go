var app = require("application");
var view = require("ui/core/view");
var frameModule = require("ui/frame");
var observableArray = require("data/observable-array");
var http = require("tns-core-modules/http");
var observableModule = require("data/observable");
var appSettings = require("application-settings");
var striptags = require('striptags');
var negocios = new observableArray.ObservableArray();

var page;
var gotData;

var id;
var url;

exports.pageLoaded = function(args) {
    page = args.object;
    gotData = page.navigationContext;

    var actionBar = view.getViewById(page, "actionBar");
    actionBar.title = gotData.subcategoria;


    if (gotData.all) {
        id = gotData.id;
        url = "http://www.mustgo.com.mx/api/negocios/getByCat/" + id;
        actionBar.title = gotData.categoria
    } else {
        id = gotData.idsubcategory;
        url = "http://mustgo.com.mx/api/negocios/getBySubCat/" + id;
    }

    negocios.splice(0, negocios.length);

    http.getJSON(url).then((res) => {
        for (var i = 0; i < res.length; i++) {
            var id = res[i].id;
            var name = res[i].nombre;
            var path
            if (gotData.subcategoria != undefined)
                path = gotData.categoria + " > " + gotData.subcategoria;
            else
                path = gotData.categoria + " > " + res[i].subCategoriaNegocio[0].nombre;
            var description = striptags(res[i].descripcion, [], '');
            if (description.length > 180)
                description = description.substring(0, 180).concat("...");
            var phone = String.fromCharCode(0xf095) + res[i].telefono;
            var rating = "";
            for (var j = 1; j <= res[i].calificacion; j++)
                rating = rating + String.fromCharCode(0xf005);
            var price = "";
            for (var k = 1; k <= res[i].precio; k++)
                price = price + '$';
            var iconList = res[i].logo;
            var portada = res[i].portada;
            var distance = getDistance(res[i].cordx, res[i].cordy);
            distance = distance.toFixed(1);
            var direccion = res[i].direccion;

            var estacionamiento = res[i].estacionamiento;
            var wifi = res[i].WIFI;
            var cc = res[i].CC;
            var domicilio = res[i].domicilio;
            var discapacitados = res[i].discapacitados;
            var mascotas = res[i].mascotas;
            var idiomas = res[i].idiomas;
            var fumar = res[i].fumar;
            var emergencias = res[i].emergencias;
            var alberca = res[i].alberca;

            var promocion = res[i].promocion;
            var recomendado = res[i].recomendado;

            var coordx = res[i].cordx;
            var coordy = res[i].cordy;

            var facebookPub = res[i].facebookPub;
            var horarios = res[i].horarios;
            var visitas = res[i].visitas;

            var fav;
            if (recomendado == 0)
                fav = String.fromCharCode(0xf087)
            else if (recomendado == 1)
                fav = String.fromCharCode(0xf164)

            negocios.push({
                id: id,
                imgProduct: iconList,
                title: name,
                path: path,
                description: description,
                telefono: phone,
                distance: distance,
                rating: rating,
                price: price,
                fav: fav,
                coordx: coordx,
                coordy: coordy,
                portada: portada,
                cc: cc,
                wifi: wifi,
                estacionamiento: estacionamiento,
                facebookPub: facebookPub,
                horarios: horarios,
                visitas: visitas,
                direccion: direccion,
                promocion: promocion,
                recomendado: recomendado,
                domicilio: domicilio,
                discapacitados: discapacitados,
                mascotas: mascotas,
                idiomas: idiomas,
                fumar: fumar,
                emergencias: emergencias,
                alberca: alberca
            });
        }
    }, function(err) {
        console.log("Error:" + err);
    });

    page.bindingContext = { myNegocios: negocios };
}

exports.viewItem = function(args) {
    var itemIndex = args.index;
    var id = negocios.getItem(itemIndex).id;
    var imgProduct = negocios.getItem(itemIndex).imgProduct;
    var title = negocios.getItem(itemIndex).title;
    var path = negocios.getItem(itemIndex).path;
    var direccion = negocios.getItem(itemIndex).direccion;
    var description = negocios.getItem(itemIndex).description;
    var telefono = negocios.getItem(itemIndex).telefono;
    var rating = negocios.getItem(itemIndex).rating;
    var price = negocios.getItem(itemIndex).price;
    var fav = negocios.getItem(itemIndex).fav;
    var coordx = parseFloat(negocios.getItem(itemIndex).coordx);
    var coordy = parseFloat(negocios.getItem(itemIndex).coordy);
    var portada = negocios.getItem(itemIndex).portada;
    var cc = negocios.getItem(itemIndex).cc;
    var wifi = negocios.getItem(itemIndex).wifi;
    var estacionamiento = negocios.getItem(itemIndex).estacionamiento;
    var domicilio = negocios.getItem(itemIndex).domicilio;
    var discapacitados = negocios.getItem(itemIndex).discapacitados;
    var mascotas = negocios.getItem(itemIndex).mascotas;
    var idiomas = negocios.getItem(itemIndex).idiomas;
    var fumar = negocios.getItem(itemIndex).fumar;
    var emergencias = negocios.getItem(itemIndex).emergencias;
    var alberca = negocios.getItem(itemIndex).alberca;
    var facebookPub = negocios.getItem(itemIndex).facebookPub;
    var horarios = negocios.getItem(itemIndex).horarios;
    var visitas = negocios.getItem(itemIndex).visitas;

    appSettings.setNumber("coordx", coordx);
    appSettings.setNumber("coordy", coordy);

    var navigationOptions = {
        moduleName: '/views/item/item',
        context: {
            id,
            imgProduct,
            title,
            path,
            direccion,
            description,
            telefono,
            rating,
            price,
            fav,
            coordx,
            coordy,
            portada,
            cc,
            wifi,
            estacionamiento,
            domicilio,
            discapacitados,
            mascotas,
            idiomas,
            fumar,
            emergencias,
            alberca,
            facebookPub,
            horarios,
            visitas,
            itemIndex
        }
    }
    frameModule.topmost().navigate(navigationOptions);
};

function getDistance(lat2, lon2) {

    var lat1 = appSettings.getNumber("currentLocationLat");
    var lon1 = appSettings.getNumber("currentLocationLong");

    var p = 0.017453292519943295; // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p)) / 2;

    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}


exports.onNavBtnTap = function() {
    console.log("Navigation button tapped!");
    frameModule.topmost().goBack();
}