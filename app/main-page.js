var geolocation = require("nativescript-geolocation");
var app = require("application");
var platform = require("platform");
var localNotifications = require("nativescript-local-notifications");
var frameModule = require("ui/frame");
var color = require("color");
var firebase = require("nativescript-plugin-firebase");
var appSettings = require("application-settings");
var connectivity = require("connectivity");
var view = require("ui/core/view");
var orientationModule = require("nativescript-screen-orientation");

var page;
var button;
var texto;
var texto1;


exports.pageLoaded = function(args) {
    page = args.object;
    page.actionBarHidden = true;

    orientationModule.setCurrentOrientation("portrait");

    texto = view.getViewById(page, "texto");
    texto1 = view.getViewById(page, "texto1");


    var connectionType = connectivity.getConnectionType();

    var connection = true;

    switch (connectionType) {
        case connectivity.connectionType.none:
            connection = false;
            console.log("No connection");
            break;
        case connectivity.connectionType:
            connection = true;
            console.log("WIFI");
            break;
        case connectivity.connectionType.mobile:
            connection = true;
            console.log("Mobile Data");
            break;
    }

    if (connection == true) {
        if (localNotifications.hasPermission()) {
            var navigationEntry = {
                moduleName: "views/login/login",
                clearHistory: true,
                animated: true,
                transition: {
                    name: "slide",
                    duration: 0,
                    curve: "easeIn"
                }
            };
            frameModule.topmost().navigate(navigationEntry);
        }
    } else {
        frameModule.topmost().navigate("views/connection/connection");
    }
};

function enableLocationTap(args) {
    button = args.object;
    if (!geolocation.isEnabled()) { // Pide LOCALIZACION
        geolocation.enableLocationRequest();
        texto.text = "Queremos informarte cuando estés cerca de algún lugar que te pueda interesar";
        texto1.text = "No pierdas detalle de las mejores opciones alrededor de ti. Nada de spam, claro está."
        button.text = String.fromCharCode(0xf0f3) + " PERMITIR NOTIFICACIONES";
    } else { // Pide Notificaciones si Localicazacion esta acitivada
        geolocation.enableLocationRequest();
        texto.text = "Queremos informarte cuando estés cerca de algún lugar que te pueda interesar";
        texto1.text = "No pierdas detalle de las mejores opciones alrededor de ti. Nada de spam, claro está."
        button.text = String.fromCharCode(0xf0f3) + " PERMITIR NOTIFICACIONES";
        localNotifications.requestPermission();
        var navigationEntry = {
            moduleName: "views/login/login",
            clearHistory: true
        };
        frameModule.topmost().navigate(navigationEntry);
    }
}
exports.enableLocationTap = enableLocationTap;

exports.noPermission = function() {
    var navigationEntry = {
        moduleName: "views/login/login",
        clearHistory: true
    };
    frameModule.topmost().navigate(navigationEntry);
}